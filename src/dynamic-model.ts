import Model from './model';
import Nongo from './nongo';

export default abstract class DynamicModel extends Model {

  constructor(nongo: Nongo, obj: any) {
    super(nongo, obj);
    this.dynamic = true;
  }

}
