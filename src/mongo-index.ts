import {IndexOptions} from 'mongodb';

/**
 * Very basic model used to hold information on a mongo index.
 */
export default class MongoIndex {

  public readonly index: any;
  public readonly name: string;

  /**
   * @param index - the index definition as you would give on the mongo shell
   * @param options IndexOptions - options for the index these match the options defined here:
   *                       https://mongodb.github.io/node-mongodb-native/api-generated/collection.html#ensureindex
   */
  constructor(index: {[key: string]: 1 | -1 | 'text'}, public readonly options: IndexOptions = {}) {
    // Delete any keys from options that have an undefined value. We do this to standardise the options so they can
    // easily be compared
    for (const key of Object.keys(options)) {
      if (options[key] === undefined) {
        delete options[key];
      }
    }

    // Define the index
    this.index = index;

    if (options.name) {
      this.name = options.name;
    } else {
      // Build the name for the index
      const nameBuilder = [];
      for (const key of Object.keys(index)) {

        // Check the order value is correct
        const order = index[key];
        if (![-1, 1, 'text'].includes(order)) {
          throw Error('order must have the value 1, -1 or "text"');
        }

        // Build the name
        nameBuilder.push(`${key}_${order}`);
      }
      this.name = nameBuilder.join('_');
    }

    // throw an error when index name is too long
    if (this.name.length > 60) {
      throw new Error(`index name "${this.name}" is too long. Maximum is 60 characters. If this is a generated name, you may try providing a shorter name in the options`);
    }

  }

  public toString() {
    return JSON.stringify(this.index) + ' ' + JSON.stringify(this.options);
  }

}
