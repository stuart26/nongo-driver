export default interface NongoParams {
  id?: string;
  protocol?: string;
  host: string;
  port: number;
  username?: string;
  password?: string;
  db: string;
  options?: any;
}
