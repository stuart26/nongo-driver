import deepEqual from 'deep-equal';
import {Collection, Db, MongoClient, ObjectId} from 'mongodb';
import Logger from 'wbb-logger';
import {Newable} from './interfaces';
import Model from './model';
import MongoIndex from './mongo-index';
import MongoIndexSet from './mongo-index-set';
import NongoParams from './nongo-params';
import SchemaParser from './schema-parser';

const logger = new Logger('nongo.ts');

export default class Nongo {
  /*
   * @return a new ObjectId if {@code id} is null, an ObjectId created using {@code id}.
   */
  public static toObjectId(id) {
    // Set the object id if there isn't one already
    if (id == null) {
      id = new ObjectId();

      // If {@code this.obj._id} is not an {@code ObjectId} turn it into one
    } else if (!(id instanceof ObjectId)) {
      id = new ObjectId(id);
    }

    return id;
  }

  public schema: any = {};
  public validKeys: any = {};
  public noStripKeys: any = {};
  public db: Db;
  public dbName: string;
  public mongoClient: MongoClient;

  // Parallel to {@code this.modelClasses} and {@code this.schemaParsers}
  private instances: Model[];

  // Map of collection names to their relative instance of Model
  private instanceMap: {
    [key: string]: Model;
  } = {};

  // Map of collection names to a function that when called returns a newly created instance of the appropriate Model
  private newGeneratorMap: {
    [key: string]: (obj: any) => Model;
  } = {};

  // {@code modelClasses} should actually be of type {@code Model[]} throws a
  // compiler error when calling {@code Class.name}
  constructor(
    private params: NongoParams,
    private modelClasses: Newable[],
    public schemaParsers?: SchemaParser[],
  ) {
  }

  public async connect() {
    // Connect to the database
    this.mongoClient = await MongoClient.connect(this.uri(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    this.db = this.mongoClient.db(this.params.db);
    this.dbName = this.params.db;

    // Map the classes to instances
    this.instances = this.modelClasses.map((Clazz) => new Clazz(this, null));

    // Init the schema parsers if they are aren't already given
    if (!this.schemaParsers) {
      this.schemaParsers = this.instances.map((instance) => {
        const collectionName = instance.name;
        // Validate the schema (throws error if not valid)
        return new SchemaParser(instance.defineSchema(), collectionName);
      });
    }

    // Iterate over each of the model classes
    for (let i = 0; i < this.instances.length; i++) {
      const Clazz = this.modelClasses[i];
      const instance = this.instances[i];
      const parser = this.schemaParsers[i];
      const schema = parser.schema;

      // Create a field on {@code this} that can be used to access what appear to
      // client code to be static methods of the model e.g. {@code nongo.MyModel.find(..)}
      this.instanceMap[instance.name] = instance;

      // Create a field on {@code this.validKeys} for the model
      this.validKeys[instance.name] = parser.validKeys;
      this.noStripKeys[instance.name] = parser.noStripKeys;

      // Create a field on {@code this.schema} for the model
      this.schema[instance.name] = schema;

      // Create the collection if it doesn't exist
      const collName = instance.collection;
      if (!(await this.collectionExisits(collName))) {
        await this.db.createCollection(collName);
      }

      // Add field to {@code this.newGenerator} with a function as a value that can be
      // used to init a new model
      this.newGeneratorMap[instance.name] = (obj) => {
        return new Clazz(this, obj);
      };
    }

    return this;
  }

  public async ensureIndexes(): Promise<void> {
    // Iterate over each of the model instances we have so we can ensure their indexes
    for (let i = 0; i < this.instances.length; i++) {
      const instance = this.instances[i];
      const parser = this.schemaParsers[i];
      const schema = parser.schema;

      // Ensure the indexes for the collection
      const explicitIndexes = instance.getMongoIndexes();
      const collName = instance.collection;
      try {
        await this.ensureCollectionIndexes(schema, collName, explicitIndexes);
      } catch (err) {
        logger.error(`Failed to ensure indexes for ${collName}`);
        logger.error(err);
      }
    }
  }

  public async dropDatabase() {
    await this.db.dropDatabase();
  }

  /**
   * Remove all documents in the database
   */
  public async removeAll() {
    for (const instance of this.instances || []) {
      await instance.remove({});
    }
  }

  public async dbExists() {
    const allDbs = (await this.db.admin().listDatabases()).databases;
    return allDbs.some((db) => db.name === this.dbName);
  }

  public async collectionExisits(collName: string) {
    const collections = await this.db.listCollections().toArray();
    return (
      (await this.dbExists()) &&
      collections.some((col) => col.name === collName)
    );
  }

  /**
   * Used to return the "collection instance" of a {@link Model}. You should use this method to gain access to an object
   * which you can use to run queries again a collection such as {Model.find(..)} etc.
   *
   * @param {{new(...args: any[]): T}} model
   * @returns {T}
   */
  public col<T extends Model>(model: new (...args: any[]) => T): T {
    return this.instanceMap[model.name] as T;
  }

  /**
   * Used to create a new instance of a {@link Model}
   *
   * @param {{new(...args: any[]): T}} model
   * @param {T["obj"]} obj
   * @returns {T}
   */
  public new<T extends Model>(
    model: new (...args: any[]) => T,
    obj: T['obj'],
  ): T {
    return this.newGeneratorMap[model.name](obj) as T;
  }

  /**
   * @returns {string} the mongo uri built from {@code this.params}.
   */
  public uri() {
    // Protocol
    let url = (this.params.protocol || 'mongodb') + '://';

    // Authentication (optional)
    if (this.params.username) {
      url += this.params.username + ':' + this.params.password + '@';
    }

    // Host
    url += this.params.host;
    // Then optionally the port

    // Port (optional)
    if (this.params.port) {
      url += ':' + this.params.port;
    }

    // Database
    url += '/' + this.params.db + '?';

    // Options given as uri params
    const options = this.params.options || {};
    url += Object.keys(options)
      .map((key) => key + '=' + encodeURI(options[key]))
      .join('&');

    return url;
  }

  private async ensureCollectionIndexes(
    schema: any,
    collName: string,
    explicitIndexes: MongoIndex[],
  ) {
    const context = this.dbName + '.' + collName;

    // Get the existing set of indexes, we will remove the ones we want to keep
    const existingIndexes = await this.existingIndexes(collName);

    // Ensure all the required indexes have been applied
    const indexSet = await this.getSchemaIndexes(schema);
    explicitIndexes.forEach((idx) => indexSet.add(idx));

    const collection = this.db.collection(collName);

    // Remove any indexes that are no longer required
    for (const index of existingIndexes.values()) {
      if (!indexSet.get(index)) {
        logger.info(
          `${context} dropping ${index} as it is no longer required...`,
        );
        try {
          await collection.dropIndex(index.name);
        } catch (e) {
          logger.error(e.message);
        }
      }
    }

    for (const index of indexSet.values()) {
      // Always background indexes
      index.options.background = true;

      // Get the existing index index with the same key if there is one
      const existingIndex = existingIndexes.get(index);

      // If there isn't log that we are creating a new one
      if (!existingIndex) {
        logger.info(`${context} is creating ${index}...`);
      }

      // If there is an existing index with same name and different config, drop it
      const replacementRequired =
        existingIndex && !deepEqual(index, existingIndex);
      if (replacementRequired) {
        logger.info(
          `${context} is replacing ${existingIndex} with ${index}...`,
        );
        await collection.dropIndex(existingIndex.name);
      }

      // Ensure the index
      await (collection as any).createIndex(index.index, index.options);

      // Wait for the background indexing to be complete
      await this.waitForIndex(collection, index);

      // If we have built an index do some logging
      if (replacementRequired || !existingIndex) {
        logger.info(`${context} finished building ${index}`);
      }
    }
  }

  private async waitForIndex(
    collection: Collection,
    index: MongoIndex,
  ): Promise<void> {
    let indexingCompete = false;
    while (indexingCompete === false) {
      const indexNames = (
        await collection.indexInformation({full: true, session: null})
      ).map((idx) => idx.name);
      indexingCompete = indexNames.includes(index.name);

      // If not done indexes wait for a second to check again
      if (!indexingCompete) {
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
    }
  }

  private async existingIndexes(collName: string) {
    const indexSet = new MongoIndexSet();

    // If the collection exists
    if (await this.collectionExisits(collName)) {
      // Iterate over the existing indexes
      const collection = this.db.collection(collName);
      for (const indexInfo of await collection.indexes()) {
        // Add all the indexes to the {@code indexSet} except _id
        const index = indexInfo.key;
        if (!deepEqual(index, {_id: 1})) {
          // Parse the index info
          const options = {
            unique: indexInfo.unique,
            background: indexInfo.background,
            name: indexInfo.name,
            weights: indexInfo.weights,
          };
          // if it's text index, retrieve the field names info from weights
          if (indexInfo.textIndexVersion) {
            const textIndex: {[k: string]: 'text'} = {};
            for (const fieldName of Object.keys(indexInfo.weights)) {
              textIndex[fieldName] = 'text';
            }
            indexSet.add(new MongoIndex(textIndex, options));
          } else {
            indexSet.add(new MongoIndex(index, options));
          }
        }
      }
    }

    return indexSet;
  }

  /**
   * Recursively traverse the schema to populate the {@code indexList} with {@link MongoIndex}s constructed using schema
   * kv-pairs.
   *
   * @param parentSchema
   * @param {string} parentPath
   * @param indexSet
   * @returns {Promise<MongoIndex[]>}
   */
  private async getSchemaIndexes(
    parentSchema: any,
    parentPath = '',
    indexSet = new MongoIndexSet(),
  ): Promise<MongoIndexSet> {
    for (const key of Object.keys(parentSchema)) {
      // Define the index
      const path = parentPath ? parentPath + '.' + key : key;

      // Create an index and add it to the index list if required. Indexes default to background
      const schema = parentSchema[key];
      if (schema.unique || schema.indexed || schema.text) {
        const index = {};
        index[path] = schema.text ? 'text' : 1;
        indexSet.add(
          new MongoIndex(index, {background: true, unique: schema.unique}),
        );
      }

      // Make a recursive call if required
      const type = schema.type;
      if (Array.isArray(type)) {
        const elType = type[0];
        if (typeof elType === 'object') {
          await this.getSchemaIndexes(elType, path, indexSet);
        }
      } else if (typeof type === 'object') {
        await this.getSchemaIndexes(type, path, indexSet);
      }
    }

    return indexSet;
  }
}
