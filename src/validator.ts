import isDate from 'is-date-object';
import Nongo from './nongo';

export default class Validator {

  public errs: string[] = [];
  private obj: any;

  constructor(private schema: any, private model: any) {
    this.obj = model.obj;
  }

  /*
   * Call this method to validate {@code this.obj} using {@code this.schema}
   * (both are provided by the constructor)
   *
   * @return an array or error messages or null if the object is validate
   */
  public async validate() {
    await this.doValidate(null, this.schema, this.obj);
    return this.errs.length > 0 ? this.errs : null;
  }

  /*
   * Recursively validates the children of {@param parent}.
   *
   * @param parentPath: the path to {@param obj} from {@code this.obj}.
   *                    e.g. "person.address.address1"
   * @param parentSchema: the schema for {@param parent}.
   * @param parent: the object that will have it's children validated.
   */
  public async doValidate(parentPath: string, parentSchema: any, parent: any) {
    for (const key of Object.keys(parentSchema)) {
      // Define the path to the current key
      const path = parentPath ? parentPath + '.' + key : key;

      // Get the value
      const value = parent[key];
      const schema = parentSchema[key];

      const type = schema.type;

      // If the value is empty and it should be move on to the next one
      if (!this.validateNotEmpty(schema, value, path)) {
        continue;
      }

      // If the value is not set move on to the next one
      if (!this.valueIsSet(schema, value, path)) {
        continue;
      }

      // If the value is not unique and it should be move on to the next one
      const isUnique = await this.validateUnique(schema, value, path);
      if (!isUnique) {
        continue;
      }

      // Primitive type
      if (typeof type === 'string' && this.primitiveType(schema, value, path)) {
        this.validateValue(schema, value, path);

        // Array
      } else if (Array.isArray(type)) {

        // Make sure the value is an array
        if (!Array.isArray(value)) {
          this.addError('***path*** must be an array', path);
          continue;
        }

        // Element type
        const elType = type[0];

        // Array of primitives
        if (typeof elType === 'string') {
          if (this.primitiveArrayType(elType, value, path)) {
            this.validateValue(schema, value, path);
          }

          // Array of objects
        } else if (this.validateValue(schema, value, path)) {
          for (const childVal of value) {
            await this.doValidate(path, elType, childVal);
          }
        }

        // Nested object
      } else if (typeof type === 'object' && this.validateValue(schema, value, path)) {

        // Make sure the value is an object
        if (typeof value !== 'object') {
          this.addError('***path*** must be an object', path);
          continue;
        }

        await this.doValidate(path, type, value);
      }
    }
  }

  /*
   * Check that there are no other objects that have the same value for {@code path}
   */
  public async validateUnique(schema, value, path) {
    // If unique has been set
    if (typeof schema.unique !== 'undefined') {

      // Process the schema
      const defaultErr = '***path*** has a unique index and "' + value + '" already exists in it';
      const processed =
        this.processSchemaField(schema.unique, value, defaultErr);
      const unique = processed[0];
      const err = processed[1];

      // If should be unique
      if (unique) {

        // Try to find a differnt doc with the same value
        const query = {
          _id: {
            $ne: Nongo.toObjectId(this.obj._id),
          },
        };
        query[path] = value;
        const doc = await this.model.findOne(query);

        // If found add error
        if (doc) {
          this.addError(err, path);
          return false;
        }

      }

    }

    return true;
  }

  public validateValue(schema, value, path) {
    if (typeof schema.validate !== 'undefined') {

      // Process the schema
      const defaultErr = value + ' is not a valid value for "***path***"';
      const processed =
        this.processSchemaField(schema.validate, value, defaultErr);
      const valid = processed[0];
      const error = processed[1];

      if (!valid) {
        this.addError(error, path);
        return false;
      }

    }

    return true;
  }

  public validateNotEmpty(schema, value, path) {
    // Check if notEmpty was set
    if (typeof schema.notEmpty !== 'undefined') {

      // Process the schema
      const processed =
        this.processSchemaField(schema.notEmpty, value, '"***path***" cannot be empty');
      const notEmpty = processed[0];
      const error = processed[1];

      // Test the value
      if (notEmpty && (value == null || value.length <= 0)) {
        this.addError(error, path);
        return false;
      }

    }

    return true;
  }

  /*
   * Checks to see if {@param value} is set adding an error it
   * should be and is not.
   *
   * @param schema: the schema for {@param value}.
   * @param value: the value that you are checking is set.
   */
  public valueIsSet(schema, value, path) {
    // If no value has been set
    if (value == null) {

      // Validate the required field
      if (schema.required) {

        // Process the schema
        const processed =
          this.processSchemaField(schema.required, value, '"***path***" is required');
        const required = processed[0];
        const error = processed[1];

        // If is required and not set add an error to the list
        if (required) {
          this.addError(error, path);
        }

      }

      return false;

    }

    return true;
  }

  public primitiveType(schema, value, path) {
    const type = schema.type;
    const isValid = this.correctPrimitive(type, value);
    if (!isValid) {
      this.addError('"***path***" must be of type "' + type + '".', path);
    }
    return isValid;
  }

  public correctPrimitive(type: string, value): boolean {
    // Check if any
    return type === 'any' ||
      // Check if typeof is correct
      typeof value === type ||
      // Check for date
      type === 'date' && isDate(value);
  }

  public primitiveArrayType(type, array, path) {
    const error = '"' + path + '" must be an array of "' + type + '" elements.';

    // Check {@code array} is an array
    if (!Array.isArray(array)) {
      this.addError(error, path);
      return false;
    }

    // If type is "any" dont need to check elements
    if (type !== 'any') {

      // Check each element in the array is the correct type
      for (const el of array) {
        if (!this.correctPrimitive(type, el)) {
          this.addError(error, path);
          return false;
        }
      }

    }

    return true;
  }

  private processSchemaField(schemaField: any, val: any, defaultErr: string): any[] {
    const processed = [];
    if (Array.isArray(schemaField)) {
      processed[0]
        = typeof schemaField[0] === 'boolean' ? schemaField[0] : schemaField[0](val, this.obj);
      processed[1]
        = typeof schemaField[1] === 'string' ? schemaField[1] : schemaField[1](val, this.obj);
    } else {
      processed[0] = typeof schemaField === 'boolean' ? schemaField : schemaField(val, this.obj);
      processed[1] = defaultErr;
    }

    return processed;
  }

  private addError(error: string, path: string) {
    error = error.replace(/\*\*\*path\*\*\*/g, path);
    this.errs.push(error);
  }

}
