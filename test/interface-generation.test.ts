import fs from 'fs';
import generateInterfaces from '../src/generate-interfaces';
import TsInterfaceGenerator from '../src/ts-interface-generator';
import {DummyModelWithChildSchema} from './models/dummy-model-with-child-schema';
import DummyModel from './models/dummy-model.nongo';

describe('Testing interface generation...', () => {

  it('Test generating DummyModelObj', async () => {
    const file = 'temp-interface.ts';
    new TsInterfaceGenerator().generateInterface(DummyModel, file);
    const code = fs.readFileSync(file, 'utf8');
    const expected =
`// This file was generated using nongo-driver's TsInterfaceGenerator.
export default interface DummyModelObj {
  'dontStripChildren'?: any;
  'dontStripChildren2'?: any;
  'exampleMap'?: {[key: string]: {
    'mapValueField': string;
  }};
  'created'?: Date;
  'arrayOfObject'?: object[];
  'name': string;
  'age': number;
  'pets': Array<{
    'species'?: string;
    'likes'?: {
      'food'?: string[];
      'drink'?: string[];
    };
  }>;
  'job': {
    'role': string;
    'at'?: string;
  };
  'location'?: {
    'address1'?: string;
  };
  'autoInit': {
    'initArray': Array<{
      'nestedObj': any;
    }>;
    'initNestedObj'?: {
      'hello'?: string;
    };
    'initNestedNative'?: any;
  };
  'notEmptyFields'?: {
    'aString': string;
    'anArray': string[];
  };
  '_id'?: any;
}
`;

    expect(code).toEqual(expected);
    fs.unlinkSync(file);
  });

  it('Test generating DummyModelObj', async () => {
    await generateInterfaces('./models');
  });

  it('Test generating DummyModelWithChildSchema', async () => {
    const file = 'temp-interface.ts';
    new TsInterfaceGenerator().generateInterface(DummyModelWithChildSchema, file);
    const code = fs.readFileSync(file, 'utf8');
    const expected =
`// This file was generated using nongo-driver's TsInterfaceGenerator.
export default interface DummyModelWithChildSchemaObj {
  'dummy'?: {
    'dontStripChildren'?: any;
    'dontStripChildren2'?: any;
    'exampleMap'?: {[key: string]: {
      'mapValueField': string;
    }};
    'created'?: Date;
    'arrayOfObject'?: object[];
    'name': string;
    'age': number;
    'pets': Array<{
      'species'?: string;
      'likes'?: {
        'food'?: string[];
        'drink'?: string[];
      };
    }>;
    'job': {
      'role': string;
      'at'?: string;
    };
    'location'?: {
      'address1'?: string;
    };
    'autoInit': {
      'initArray': Array<{
        'nestedObj': any;
      }>;
      'initNestedObj'?: {
        'hello'?: string;
      };
      'initNestedNative'?: any;
    };
    'notEmptyFields'?: {
      'aString': string;
      'anArray': string[];
    };
  };
  '_id'?: any;
}
`;

    expect(code).toBe(expected);
    fs.unlinkSync(file);
  });

});
