import Model from '../../src/model';
import Nongo from '../../src/nongo';
import DummyModelWithMaxIndexObj from './dummy-model-with-max-index-obj';

export default class DummyModelWithMaxIndex extends Model<DummyModelWithMaxIndexObj> {

    private static getClientTypes() {
        // generate 63 properties with an index each in addition to _id
        // max allowed indexes is 64
        const clientTypes: {} = {};
        for (let i = 1; i <= 63; i++) {
            clientTypes[`client${i}`] = {
                type: {
                    favouritePet: {type: 'string', indexed: true},
                },
            };
        }
        return clientTypes;
    }

    constructor(nongo: Nongo, obj: any) {
        super(nongo, obj);
    }

    public defineSchema(): any {
        return {
            clients: {
                type: DummyModelWithMaxIndex.getClientTypes(),
            },
        };
    }

    protected defineCollection(): string {
        return 'DummyModel';
    }
}
