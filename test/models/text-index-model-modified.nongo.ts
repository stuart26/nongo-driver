import Nongo from '../../src/nongo';
import TextIndexModel from './text-index-model.nongo';

export default class TextIndexModelModified extends TextIndexModel {

  constructor(nongo: Nongo, obj: any) {
    super(nongo, obj);
    // add an additional language
    this.languages.push('es');
    // this should cause the schema and the text index to change
  }
}
