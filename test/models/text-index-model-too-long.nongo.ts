import {MongoIndex} from '../../src';
import TextIndexModel from './text-index-model.nongo';

export default class TextIndexModelTooLong extends TextIndexModel {
  public getMongoIndexes(): MongoIndex[] {
    const textIndexes = {};
    this.languages.forEach((lang) => {
      textIndexes['description.' + lang] = 'text';
    });
    return [new MongoIndex({
      name: 'text',
      summary: 'text',
      ...textIndexes,
    })];
  }
}
