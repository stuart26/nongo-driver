import {Model, MongoIndex} from '../../src';

export default class TextIndexModel extends Model<any> {
  protected languages = ['en', 'cy'];

  public defineSchema(): any {
    // simulate adding text index for multiple nested fields
    const languageSchema = {};

    this.languages.forEach((lang) => {
      languageSchema[lang] = {type: 'string'};
    });

    return {
      name: {
        type: 'string',
      },
      module: {
        type: 'string',
      },
      description: {
        type: languageSchema,
      },
      summary: {
        type: 'string',
      },
    };
  }

  public getMongoIndexes(): MongoIndex[] {
    const textIndexes = {};
    this.languages.forEach((lang) => {
      textIndexes['description.' + lang] = 'text';
    });
    return [new MongoIndex({name: 1, module: 1}, {unique: true}), new MongoIndex({
      name: 'text',
      summary: 'text',
      ...textIndexes,
    }, {name: 'my_text_index'})];
  }

  protected defineCollection(): string {
    return 'TextIndexModel';
  }
}
