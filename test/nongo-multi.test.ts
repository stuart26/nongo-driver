import {MongoClient} from 'mongodb';
import Logger from 'wbb-logger';
import NongoMulti from '../src/nongo-multi';
import DummyModel from './models/dummy-model.nongo';

const logger = new Logger('nongo-multi-test.ts');

// Catch unhanded promise rejections and log them
process.on('unhandledRejection', (reason, p) => {
  logger.error(reason);
});

const dbNames = [
  'nodeunittest-nongo-multi-1',
  'nodeunittest-nongo-multi-2',
  'nodeunittest-nongo-multi-3',
];

async function drop() {
  for (const dbName of dbNames) {
    const client = await MongoClient.connect('mongodb://localhost:27017/', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    const db = client.db(dbName);
    await db.dropDatabase();
    await client.close();
  }
}

const models = [DummyModel];

describe('Testing NongoMulti...', () => {
  beforeEach(async () => await drop());
  afterEach(async () => await drop());

  /*
   * Tests finding objects. We can start using nongo save here as we have
   * confirmed that it has worked in the previous test
   */
  it('Test connect', async () => {
    const nameToId = (dbName) => dbName + '-id';

    // Create an array of params one for each db name
    const paramsArray = dbNames.map((dbName) => ({
      id: nameToId(dbName),
      host: 'localhost',
      port: 27017,
      db: dbName,
    }));

    // Connect a nongo multi instance
    const nongoMulti = await new NongoMulti(paramsArray, models).connect();

    // Test the connections are configured correctly
    for (const dbName of dbNames) {
      const nongo = nongoMulti.getNongo(nameToId(dbName));
      expect(dbName).toBe(nongo.dbName);
    }
  });
});
