import clone from 'clone-deep';
import fs from 'fs';
import {ObjectId} from 'mongodb';
import rimraf from 'rimraf';
import Logger from 'wbb-logger';
import {Nongo} from '../src';
import DatabaseHelper from './database-helper';
import DummyModelChanged from './models/dummy-model-changed.nongo';
import DummyModelWithMaxIndex from './models/dummy-model-with-max-index.nongo';
import DummyModel from './models/dummy-model.nongo';
import TextIndexModelModified from './models/text-index-model-modified.nongo';
import TextIndexModelTooLong from './models/text-index-model-too-long.nongo';
import TextIndexModel from './models/text-index-model.nongo';

const logger = new Logger('nongo-test.ts');

// Catch unhanded promise rejections and log them
process.on('unhandledRejection', (reason) => {
  logger.error(reason);
});

async function find() {
  const client = await DatabaseHelper.getClient();
  const db = client.db(await DatabaseHelper.getDbName());
  return await db.collection('DummyModel').find({}).toArray();
}

const models = [DummyModel];
const pets = [
  {
    species: 'dog',
  },
  {
    species: 'dog',
  },
];
const job = {
  role: 'dogsbody',
  at: 'bing',
};

const obj1: any = {
  name: 'obj1',
  age: 123,
  created: new Date('2014-01-22T14:56:59.301Z'),
  dontStripChildren: {
    a: 1,
    b: 2,
  },
  dontStripChildren2: {
    c: 3,
    d: 4,
  },
  arrayOfObject: [{}, {}],
  pets,
  job,
};

const obj2: any = {
  name: 'obj2',
  age: 321,
  pets,
  job,
};

describe('Testing DB operations...', () => {

  beforeEach(async () => await DatabaseHelper.drop());

  /*
   * Tests the saving and updating of single objects
   */
  it('Test save', async () => {

    const mutableObj: any = {
      name: 'mutable',
      age: 123456,
      pets,
      job,
    };

    const nongo = await DatabaseHelper.getNongoInstance(models);

    const model = await nongo.new(DummyModel, mutableObj).save();

    // Check the returned model has an _id
    expect(model.obj._id).toBeDefined();

    // Copy the _id to obj1 and check that the objects are the same
    mutableObj._id = model.obj._id;
    expect(mutableObj).toEqual(model.obj);

    // Query the database and make sure that the stored doc is correct
    let docs = await find();
    expect(docs).toHaveLength(1);
    expect(docs[0]).toEqual(mutableObj);

    // Update name
    model.obj.name = 'new name';
    // This both otherKey fields should be removed as it is not in the schema
    (model.obj as any).otherKey = 'otherKey';
    model.obj.pets = [
      {
        species: 'some species',
        otherKey: 'otherKey',
      },
      {
        species: 'another species',
      },
    ] as any;
    const presave: any = model.obj;

    const updated = await model.save();

    // Check that {@code otherKey}s are still present on presave
    expect(presave.otherKey).toBeDefined();
    expect(presave.pets[0].otherKey).toBeDefined();

    // Then remove them
    delete presave.otherKey;
    delete presave.pets[0].otherKey;

    // Check the updated model is returned correctly
    expect(model.obj).toEqual(updated.obj);

    // Query the database and make sure that the stored doc is correct
    docs = await find();
    expect(docs).toHaveLength(1);
    expect(docs[0]).toEqual(model.obj);

    // Save another object
    await nongo.new(DummyModel, obj2).save();
    docs = await find();
    expect(docs).toHaveLength(2);

    await nongo.mongoClient.close();
  });

  /*
   * Tests finding objects. We can start using nongo save here as we have
   * confirmed that it has worked in the previous test
   */
  it('Test find', async () => {
    // Save the objects
    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    await nongo.new(DummyModel, obj1).save();
    await nongo.new(DummyModel, obj2).save();

    // Find all, with options
    const options = {
      sort: {name: -1},
    };
    let docs = await nongo.col(DummyModel).find({}, options);
    expect(docs).toHaveLength(2);
    expect(docs[0].obj.name).toBe(obj2.name);
    expect(docs[1].obj.name).toBe(obj1.name);

    // Find with query
    docs = await nongo.col(DummyModel).find({name: 'obj1'});
    expect(docs).toHaveLength(1);
    expect(docs[0].obj).toEqual(obj1);

    // Test getting a cursor back
    const expected: any[] = [obj1, obj2];
    for await (const doc of await nongo.col(DummyModel).findIterator({})) {
      expect(expected.shift()).toEqual(doc.obj);
    }

    await nongo.mongoClient.close();
  });

  /*
   * Tests finding objects. We can start using nongo save here as we have
   * confirmed that it has worked in the previous test
   */
  it('Test findOne', async () => {
    // Save the objects
    const nongo: Nongo = await DatabaseHelper.getNongoInstance(models);
    await nongo.new(DummyModel, obj1).save();
    await nongo.new(DummyModel, obj2).save();

    // Find one doc
    let doc = await nongo.col(DummyModel).findOne({name: 'obj1'});

    // Check the correct doc was returned
    expect(doc.obj.name).toBe('obj1');
    expect(doc.obj.age).toBe(123);

    // Look for a doc that isn't there
    doc = await nongo.col(DummyModel).findOne({name: 'no objs with this name'});
    expect(doc).toBeNull();

    // Finish the test
    await nongo.mongoClient.close();
  });

  it('Test prepareQuery(..)', async () => {
    const nongo: Nongo = await DatabaseHelper.getNongoInstance(models);
    const dummyCol = nongo.col(DummyModel);

    const notId = {name: 'hi'};
    expect(dummyCol.prepareQuery(notId)).toEqual(notId);

    const idObjectId = {_id: new ObjectId()};
    expect(dummyCol.prepareQuery(idObjectId)).toEqual(idObjectId);

    const idString = {_id: new ObjectId().toHexString()};
    expect(dummyCol.prepareQuery(idString)).toEqual({_id: new ObjectId(idString._id)});

    const mixedIn = {_id: {$in: [new ObjectId().toHexString(), new ObjectId()]}};
    const prepared = dummyCol.prepareQuery(mixedIn);
    mixedIn._id.$in[0] = new ObjectId(mixedIn._id.$in[0]);
    expect(prepared).toEqual(mixedIn);
  });

  /*
   * Tests validating objects.
   */
  it('Test validation', async () => {
    const validObj = {
      name: 'obj1',
      age: 123,
      created: new Date(),
      pets: [
        {
          species: 'dog',
          age: 7,
          likes: {
            food: [
              'dog food',
              'human food',
            ],
            drink: [
              'water',
            ],
          },
        },
        {
          species: 'cat',
          age: 9,
          likes: {
            food: [
              'cat food',
              'human food',
            ],
            drink: [
              'water',
              'milk',
            ],
          },
        },
      ],
      job: {
        role: 'CEO',
        at: 'Pets R\' Us',
      },
      notEmptyFields: {
        aString: 'this is a string',
        anArray: ['1', '2', '3'],
      },
    };

    const invalidObj = {
      name: 'invalid name',
      created: 'should be a date',
      pets: [
        {
          species: 7,
          age: 7,
          likes: {
            food: [
              'dog food',
              'human food',
            ],
            drink: [123],
          },
        },
        {
          species: 'cat',
          age: 9,
          likes: {
            food: [
              123,
              123,
            ],
            drink: 'I should be an array',
          },
        },
      ],
      job: {
        role: 123,
      },
      location: 'should be an object',
      notEmptyFields: {
        aString: null,
        anArray: [],
      },
    };

    const nongo: any = await DatabaseHelper.getNongoInstance(models);

    // Check validation fails for invalidObj
    let errors = await nongo.new(DummyModel, invalidObj).validate();
    const expected = ['"created" must be of type "date".',
      '"invalid name" is not a valid name',
      'DummyModels must have an age',
      '"pets.species" must be of type "string".',
      '"pets.likes.food" must be an array of "string" elements.',
      '"pets.likes.drink" must be an array of "string" elements.',
      '"job.role" must be of type "string".',
      'job.at must be set if job.role is',
      'notEmptyFields.aString must be a string with at least one character',
      '"notEmptyFields.anArray" cannot be empty',
      'pets.likes.drink must be an array',
      'location must be an object',
    ];
    expect(errors.sort()).toEqual(expected.sort());

    // Check validation passes for valid object
    let validModel = nongo.new(DummyModel, clone(validObj));
    errors = await validModel.validate();
    expect(errors).toBeNull();

    // Check validation doesn't fail on unique index when it's the same model.
    // note when save returns the _id is set on valid model.
    validModel = await validModel.save();
    expect(await validModel.validate()).toBeNull();

    // Check validation fails on unique index when it's the not same model (this one has no _id)
    errors = await nongo.new(DummyModel, validObj).validate();
    expect(errors).toEqual(['name has a unique index and "obj1" already exists in it']);

    await nongo.mongoClient.close();

  });

  /*
   * Tests finding objects. We can start using nongo find and save here as they
   * have been tested above
   */
  it('Test delete', async () => {

    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    const model1 = await nongo.new(DummyModel, obj1).save();
    await nongo.new(DummyModel, obj2).save();

    // Del obj 1 (using toHexString to keep test that prepareQuery works)
    const res = await nongo.col(DummyModel).remove({_id: model1.obj._id.toHexString()});
    // Check a model was deleted
    expect(res).toEqual({n: 1, ok: 1});

    const docs = await nongo.col(DummyModel).find();

    // Make sure the correct doc was deleted
    const model = docs[0];
    expect(docs).toHaveLength(1);
    expect(model.obj.name).toBe(obj2.name);
    expect(model.obj.age).toBe(obj2.age);

    // Finish the test
    await nongo.mongoClient.close();
  });

  it('Test indexes have been applied', async () => {
    const client = await DatabaseHelper.getClient();
    const db = client.db(await DatabaseHelper.getDbName());
    const {databaseName} = db;
    const ns = `${databaseName}.DummyModel`;

    let expected: any = [
      {
        v: 2,
        key: {
          _id: 1,
        },
        name: '_id_',
        ns,
      },
      {
        v: 2,
        background: true,
        key: {
          'pets.likes.drink': 1,
        },
        name: 'pets.likes.drink_1',
        ns,
      },
      {
        v: 2,
        background: true,
        key: {
          'job.role': 1,
        },
        name: 'job.role_1',
        ns,
      },
      {
        background: true,
        key: {
          created: 1,
          dontStripChildren: 1,
        },
        name: 'dontStripChildren_1_created_1',
        ns,
        v: 2,
      },
      {
        background: true,
        default_language: 'english',
        key: {
          _fts: 'text',
          _ftsx: 1,
        },
        language_override: 'language',
        name: 'age_text',
        ns,
        textIndexVersion: 3,
        v: 2,
        weights: {
          age: 1,
        },
      },
      {
        v: 2,
        background: true,
        unique: true,
        key: {
          name: 1,
        },
        name: 'name_1',
        ns,
      },
    ];

    let nongo = await DatabaseHelper.getNongoInstance([DummyModel]);
    await nongo.ensureIndexes();
    let indexes = await db.collection('DummyModel').indexes();
    expect(indexes).toEqual(expect.arrayContaining(expected));
    await nongo.mongoClient.close();

    expected = [
      {
        v: 2,
        key: {
          _id: 1,
        },
        name: '_id_',
        ns,
      },
      {
        v: 2,
        background: true,
        key: {
          name: 1,
        },
        name: 'name_1',
        ns,
      },
      {
        v: 2,
        background: true,
        key: {
          'pets.likes.drink': 1,
        },
        name: 'pets.likes.drink_1',
        ns,
      },
      {
        background: true,
        default_language: 'english',
        key: {
          _fts: 'text',
          _ftsx: 1,
        },
        language_override: 'language',
        name: 'name_text_age_text',
        ns,
        textIndexVersion: 3,
        v: 2,
        weights: {
          age: 1,
          name: 1,
        },
      },
    ];

    nongo = await DatabaseHelper.getNongoInstance([DummyModelChanged]);
    await nongo.ensureIndexes();
    indexes = await db.collection('DummyModel').indexes();
    expect(indexes).toEqual(expect.arrayContaining(expected));
    await nongo.mongoClient.close();

    await client.close();
  });

  it(
    'Test old indexes were removed', async () => {
      const client = await DatabaseHelper.getClient();
      const db = client.db(await DatabaseHelper.getDbName());
      const {databaseName} = db;
      const ns = `${databaseName}.DummyModel`;
      const expected: any = [
        {
          key: {
            _id: 1,
          },
          name: '_id_',
          ns,
          v: 2,
        },
        {
          background: true,
          key: {
            name: 1,
          },
          name: 'name_1',
          ns,
          v: 2,
        },
        {
          background: true,
          key: {
            'pets.likes.drink': 1,
          },
          name: 'pets.likes.drink_1',
          ns,
          v: 2,
        },
        {
          background: true,
          default_language: 'english',
          key: {
            _fts: 'text',
            _ftsx: 1,
          },
          language_override: 'language',
          name: 'name_text_age_text',
          ns,
          textIndexVersion: 3,
          v: 2,
          weights: {
            age: 1,
            name: 1,
          },
        },
      ];

      // create dummy model with maximum possible number of indexes
      let nongo = await DatabaseHelper.getNongoInstance([DummyModelWithMaxIndex]);
      await nongo.ensureIndexes();
      let indexes = await db.collection('DummyModel').indexes();
      expect(indexes.length).toEqual(64);

      // create dummy model, should remove unused indexes from previous before adding new ones
      nongo = await DatabaseHelper.getNongoInstance([DummyModel]);
      await nongo.ensureIndexes();

      // should remove all unused indexes from previous
      nongo = await DatabaseHelper.getNongoInstance([DummyModelChanged]);
      await nongo.ensureIndexes();

      // check expected index
      indexes = await db.collection('DummyModel').indexes();
      expect(indexes).toEqual(expect.arrayContaining(expected));
      await nongo.mongoClient.close();

      await client.close();
    });

  it('Test aggregation', async () => {
    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    await nongo.new(DummyModel, obj1).save();
    await nongo.new(DummyModel, obj2).save();

    const pipeline = [
      {
        $group: {
          _id: '$name',
          count: {$sum: 1},
        },
      },
    ];

    const options = {
      sort: {name: -1},
    };

    const expected = [
      {
        _id: 'obj2',
        count: 1,
      },
      {
        _id: 'obj1',
        count: 1,
      },
    ];

    // Aggreate and check result is correct
    const results = await nongo.col(DummyModel).aggregate(pipeline, options);
    expect(results).toHaveLength(2);
    expect(results).toEqual(expected);

    for await (const doc of await nongo.col(DummyModel).aggregateIterator(pipeline, options)) {
      expect(expected.shift()).toEqual(doc);
    }

    await nongo.mongoClient.close();
  });

  it('Test update', async () => {
    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    await nongo.new(DummyModel, obj1).save();
    await nongo.new(DummyModel, obj2).save();

    const newAge = 999999999;
    const result = await nongo.col(DummyModel)
      .update({}, {$set: {age: newAge}});
    expect(result).toEqual({n: 2, nModified: 2, ok: 1});

    const doc = await nongo.col(DummyModel).findOne();
    expect(doc.obj.age).toBe(newAge);

    await nongo.mongoClient.close();
  });

  /*
   * Tests finding objects. We can start using nongo save here as we have
   * confirmed that it has worked in the previous test
   */
  it('Test distinct', async () => {
    // Save the objects
    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    await nongo.new(DummyModel, obj1).save();
    await nongo.new(DummyModel, obj2).save();

    let docs = await nongo.col(DummyModel).distinct('name');
    expect(docs).toHaveLength(2);
    docs = docs.sort();
    expect(docs[0]).toBe(obj1.name);
    expect(docs[1]).toBe(obj2.name);

    // Finish the test
    await nongo.mongoClient.close();
  });

  it('Test count', async () => {
    // Make sure count is 0
    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    expect(await nongo.col(DummyModel).count()).toBe(0);

    // Save the objects
    await nongo.new(DummyModel, obj1).save();
    await nongo.new(DummyModel, obj2).save();

    // Test count
    expect(await nongo.col(DummyModel).count()).toBe(2);
    expect(await nongo.col(DummyModel).count({name: obj1.name})).toBe(1);

    // Finish the test
    await nongo.mongoClient.close();
  });

  it('Test dropping database', async () => {
    // Insert and confirm it worked
    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    await nongo.new(DummyModel, obj1).save();
    expect(await nongo.col(DummyModel).findOne()).toBeDefined();

    // Drop and confirm it worked
    await nongo.dropDatabase();
    expect(await nongo.col(DummyModel).findOne()).toBeNull();

    // Finish the test
    await nongo.mongoClient.close();
  });

  it('Test removing all docs database', async () => {
    // Insert and confirm it worked
    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    await nongo.new(DummyModel, obj1).save();
    expect(await nongo.col(DummyModel).findOne()).toBeDefined();

    // Remove all and confirm it worked
    await nongo.removeAll();
    expect(await nongo.col(DummyModel).findOne()).toBeNull();

    // Make sure we still have some indexes
    expect((await nongo.db.collection('DummyModel').indexes()).length).toBeTruthy();

    // Finish the test
    await nongo.mongoClient.close();
  });

  it('Test creating collection dump', async () => {
    // Insert and confirm it worked
    const nongo: any = await DatabaseHelper.getNongoInstance(models);
    await nongo.new(DummyModel, obj1).save();
    expect(await nongo.col(DummyModel).findOne()).toBeDefined();

    const dir = './testing/temp-dump';

    // Make sure {@code dir} doesn't exist
    expect(fs.existsSync(dir)).toBeFalsy();

    await nongo.col(DummyModel).dumpCollection(dir, true);

    // Check the dir has been populated by the dump
    expect(fs.readdirSync(dir).length).toBeTruthy();

    // Clean up {@code dir}
    await new Promise(((resolve) => {
      rimraf(dir, () => {
        logger.info('Cleaned up ' + dir);
        resolve();
      });
    }));

    // Finish the test
    await nongo.mongoClient.close();
  });

});

it('test text index update', async () => {
  let nongo: any = await DatabaseHelper.getNongoInstance([TextIndexModel]);
  await nongo.ensureIndexes();

  nongo = await DatabaseHelper.getNongoInstance([TextIndexModelTooLong]);
  await expect(nongo.ensureIndexes()).rejects.toThrow('index name "name_text_summary_text_description.en_text_description.cy_text" is too long');

  nongo = await DatabaseHelper.getNongoInstance([TextIndexModelModified]);
  await nongo.ensureIndexes();

  const client = await DatabaseHelper.getClient();
  const db = client.db(await DatabaseHelper.getDbName());
  const {databaseName} = db;
  const ns = `${databaseName}.TextIndexModel`;
  const indexes = await db.collection('TextIndexModel').indexes();

  expect(indexes).toEqual([{
    v: 2,
    key: {_id: 1},
    name: '_id_',
    ns,
  },
    {
      v: 2,
      unique: true,
      key: {name: 1, module: 1},
      name: 'name_1_module_1',
      ns,
      background: true,
    },
    {
      v: 2,
      key: {_fts: 'text', _ftsx: 1},
      name: 'my_text_index',
      ns,
      background: true,
      weights:
        {
          'description.cy': 1,
          'description.en': 1,
          'description.es': 1,
          'name': 1,
          'summary': 1,
        },
      default_language: 'english',
      language_override: 'language',
      textIndexVersion: 3,
    }]);
});
