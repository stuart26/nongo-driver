import SchemaParser from '../src/schema-parser';

function validSchema() {
  return {
    name: {
      type: 'string',
      unique: [true, (val, obj) => '***path*** has a unique index and '
        + val + ' already exist in it'],
      required: [true, 'Must have a name'],
    },

    address: {

      required: [
        (val, obj) => true,
        'Must have a name',
      ],

      type: {
        address1: {
          type: 'string',
        },
      },

    },

    phone: {
      indexed: true,
      type: ['number'],
      validate: [
        (val, obj) => true,
        (val, obj) => 'This is not a valid phone number',
      ],
    },

    likes: {
      type: [
        {
          name: {
            type: 'string',
            notEmpty: true,
          },
        },
      ],
      notEmpty: [true, 'likes should not be empty'],
    },

    something: {
      type: 'any',
      unique: true,
    },

  };
}

const collection = 'testing';

describe('Testing schema validation...', () => {

  it('Test valid schema', async () => {
    // tslint:disable-next-line:no-unused-expression testing the constructor
    new SchemaParser(validSchema(), collection);
  });

  it('Test invalid required field', (done) => {
    const error = SchemaParser.standardSchemaError('name', 'required');

    let schema = validSchema();
    schema.name.required[0] = 'should be boolean or function';
    expect(() => new SchemaParser(schema, collection)).toThrow(error);

    schema = validSchema();
    // Should be a string or a function
    schema.name.required[1] = true;
    expect(() => new SchemaParser(schema, collection)).toThrow(error);

    done();
  });

  it('Test invalid type field', async () => {
    let schema = validSchema();
    delete schema.name.type;
    expect(() => new SchemaParser(schema, collection)).toThrow( 'name is missing a type field.');

    schema = validSchema();
    schema.name.type = 'not a valid type';
    expect(() => new SchemaParser(schema, collection))
      .toThrow(
        'name.type must have either string,number,boolean,date,object,any, an array or an object as it\'s value',
      );
  });

  it('Test invalid validate field', async () => {
    const error = SchemaParser.standardSchemaError('phone', 'validate');

    let schema: any = validSchema();
    schema.phone.validate = 'this should be an array with two functions.';
    expect(() => new SchemaParser(schema, collection)).toThrow(error);

    schema = validSchema();
    schema.phone.validate[0] = 'This should be a function';
    expect(() => new SchemaParser(schema, collection)).toThrow(error);

    schema = validSchema();
    schema.phone.validate.push(() => 'This is one function too many');
    expect(() => new SchemaParser(schema, collection)).toThrow(error);
  });

  it('Test invalid indexed field', async () => {
    const schema: any = validSchema();
    schema.phone.indexed = 'should be a boolean';
    expect(() => new SchemaParser(schema, collection)).toThrow('If set, phone.indexed must be a boolean');
  });

  it('Test invalid unique field', async () => {
    const schema: any = validSchema();
    schema.name.unique = 'should be an array or boolean';
    const error = SchemaParser.standardSchemaError('name', 'unique');
    expect(() => new SchemaParser(schema, collection)).toThrow(error);
  });

  it('Test invalid notEmpty field', async () => {
    let schema: any = validSchema();
    schema.name.notEmpty = 'should be an array';
    let error = SchemaParser.standardSchemaError('name', 'notEmpty');
    expect(() => new SchemaParser(schema, collection)).toThrow(error);

    schema = validSchema();
    schema.other = {
      type: 'number',
      notEmpty: [true, 'should fail as type is number'],
    };
    error = SchemaParser.notEmptyTypeError('other');
    expect(() => new SchemaParser(schema, collection)).toThrow(error);
  });

  it('Test invalid schema fields', async () => {
    const schema: any = validSchema();
    schema.phone.badField = 'this is not a valid field';
    const error = 'phone.badField is not a vaid'
      + ' schema key, should badField be within a "type" object / the root object?';
    expect(() => new SchemaParser(schema, collection)).toThrow(error);
  });

  it('Test validKeys', async () => {
    const expected = new Set([
      '_id',
      'name',
      'address',
      'address.address1',
      'phone',
      'likes',
      'likes.name',
      'something',
    ]);

    const schema: any = validSchema();
    expect(new SchemaParser(schema, collection).validKeys).toEqual(expected);
  });

});
